'use strict';

var items = [
	{title: 'Paint pots', quantity: 8, price: 3.95},
	{title: 'Polka dots', quantity: 17, price: 12.95},
	{title: 'Pebbles', quantity: 5, price: 6.95}
];

var app = angular.module('myApp', [])
	.controller('CartController', ['$scope',
		function ($scope) {			
			$scope.items = items;
			$scope.totalCart = function () {
				var total = 0, item = null;
				for (var i = 0, len = $scope.items.length; i < len; i++) {
					item = $scope.items[i];
					total = total + item.price * item.quantity;
				}
				
				return total;
			};
			$scope.subtotal = function () {
				return $scope.totalCart() - $scope.discount;
			};
			function calculateDiscount(newValue, oldValue, scope) {
				$scope.discount = newValue > 100 ? 10 : 0;
			};
			$scope.$watch($scope.totalCart, calculateDiscount);
}]);