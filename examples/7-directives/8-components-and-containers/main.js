var app = angular.module("app",[]);
 
app.directive("clock", function () {
  return {
    restrict: 'E',
    scope: {
      timezone: "@"
    },
    template: "<div>12:00pm {{timezone}}</div>"
  };
});

app.directive("panel", function () {
  return {
    restrict: "E",
    transclude: true,
    scope: {
      title: "@"
    },
    templateUrl: "templates/panel.html"
  };
});