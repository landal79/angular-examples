var myModule = angular.module(...);
myModule.directive('namespaceDirectiveName', function factory(injectables) {
	var directiveDefinitionObject = {
		// Declare how directive can be used in a template as an element, attribute, class, comment, or any combination.
		restrict: string,		
		// Set the order of execution in the template relative to other directives on the element.
		priority: number,
		// Specify an inline template as a string. Not used if you’re specifying your template as a URL.
		template: string,
		// Specify the template to be loaded by URL. This is not used if you’ve specified an inline template as a string.
		templateUrl: string,
		// If true, replace the current element. If false or unspecified, append this directive to the current element.
		replace: bool,
		// Lets you move the original children of a directive to a location inside the new template.
		transclude: bool,
		// Create a new scope for this directive rather than inheriting the parent scope.
		scope: bool or object,
		// Create a controller which publishes an API for communicating across directives.
		controller: 
			function controllerConstructor($scope, $element, $attrs, $transclude),
		// Require that another directive be present for this directive to function correctly.
		require: string,
		// Programmatically modify resulting DOM element instances, add event listeners, and set up data binding.
		link: function postLink(scope, iElement, iAttrs) {...},
		// Programmatically modify the DOM template for features across copies of a directive, as when used in ng-repeat.
        // Your compile function can also return link functions to modify the resulting element instances.
		compile: function compile(tElement, tAttrs, transclude) {
			return {
				pre: function preLink(scope, iElement, iAttrs, controller) {...},
				post: function postLink(scope, iElement, iAttrs, controller) {...}
			}
		}
	};
	return directiveDefinitionObject;
});