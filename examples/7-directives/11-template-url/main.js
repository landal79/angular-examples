var appModule = angular.module('app', []);
appModule.directive('hello', function () {
	return {
		restrict: 'E',
		templateUrl: 'templates/helloTemplate.html',
		replace: true
	};
});