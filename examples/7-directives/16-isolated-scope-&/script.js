var app = angular.module('phoneApp', []);

app.controller("AppCtrl", function ($scope) {
    $scope.callHome = function (msg) {
        alert(msg);
    };
});
app.directive("phone", function () {
    return {
        scope: {
            dial: "&"
        },
        templateUrl: 'templates/phone.html'        
    };
});