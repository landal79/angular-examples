var app = angular.module('superhero', []);

app.directive('superman', function () {
	return {
		restrict: 'E', //Element restriction
		template: '<div> Here I\'m to save the day</div>'
	};
});

app.directive('attrdirective', function () {
	return {
		restrict: "A", //Attribute restriction
		link: function () {
			alert("Attribute directive works!");
		}
	};
});

app.directive('classdirective', function () {
	return {
		restrict: "C", //Class restriction
		link: function () {
			alert("Class directive works!");
		}
	};
});

app.directive('mdirective', function () {
	return {
		restrict: "M", //comment directive
		link: function () {
			alert("Comment directive works!");
		}
	};
});