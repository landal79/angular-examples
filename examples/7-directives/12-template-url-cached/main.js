angular.module('app', [])
	.run(function ($templateCache, $http) {
		$templateCache.put('helloTemplateCached.html',
			$http.get('templates/helloTemplate.html').success(function (data) {
				return data;
			}));
	}).directive('hello', function () {
		return {
			restrict: 'E',
			templateUrl: 'helloTemplateCached.html',
			replace: true
		};
	});