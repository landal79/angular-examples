var app = angular.module('behaviorApp', []);

app.directive("enter", function () {
	/*
     * This is a shorhand for the linking function.
	 */
	return function (scope, element) {
		element.bind("mouseenter", function () {
			console.log("I'm inside of you!");
			element.addClass('highlight');
		});
	};
});

app.directive("leave", function () {
	return function (scope, element) {
		element.bind("mouseleave", function () {
			console.log("I'm leaving on a jet plane!");
			element.removeClass('highlight');
		});
	};
});