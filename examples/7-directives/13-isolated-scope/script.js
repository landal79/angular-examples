var app = angular.module('choreApp', []);

app.controller("ChoreCtrl", function($scope){
  $scope.logChore = function(chore){
    alert(chore + " is done!");
  };
});
 
app.directive("kid", function() {
  return {
    restrict: "E",
    scope: {
        done: "&"
      },
    templateUrl: 'templates/kid.html'
  };
});