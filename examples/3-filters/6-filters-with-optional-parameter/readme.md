Creating custom filters

Writing your own filter is very easy: just register a new filter factory function with your module. Internally, this uses the filterProvider. This factory function should return a new filter function which takes the input value as the first argument. Any filter arguments are passed in as additional arguments to the filter function.

The following sample filter reverses a text string. In addition, it conditionally makes the text upper-case.