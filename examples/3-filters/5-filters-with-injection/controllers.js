angular.module('coolApp', []).factory('Data', function() {
	return {
		message: "I'm data from a service"
	};
}).filter('reverse', function (Data) {
  return function (text) {
    return text.split("").reverse().join("") + " | " + Data.message;
  }
}).controller('FirstCtrl', function($scope, Data) {
	$scope.data = Data;
}).controller('SecondCtrl', function($scope, Data) {
	$scope.data = Data;	
});