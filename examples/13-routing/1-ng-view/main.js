(function () {
	'use strict';
	var app = angular.module("app", ['ngRoute']);

	app.config(function ($routeProvider) {
		$routeProvider.when("/", {
			templateUrl: "templates/app.html",
			controller: "AppCtrl"
		});
	});

	app.controller("AppCtrl", function ($scope) {
		$scope.model = {
			message: "This is my app!!!"
		};
	});
}());