(function () {
	'use strict';
	var app = angular.module("app", ['ngRoute']);

	app.config(function ($routeProvider) {
		$routeProvider
			.when('/', {
				templateUrl: "templates/app.html",
				controller: "AppCtrl"
			}).when('/pizza', {
				redirectTo: function (routeParams, path, search) {
					console.log(routeParams);
					console.log(path);
					console.log(search);
					return "/";
				}
			});
	});

	app.controller("AppCtrl", function ($scope) {
		$scope.model = {
			message: "This is my app!!!"
		};
	});
}());