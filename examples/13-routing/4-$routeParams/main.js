(function () {
	'use strict';
	var app = angular.module("app", ['ngRoute']);

	app.config(function ($routeProvider) {
		$routeProvider
			.when('/:message', {
				templateUrl: "templates/app.html",
				controller: "AppCtrl"
			}).when('/:map/:country/:state/:city', {
				templateUrl: "templates/app.html",
				controller: "App2Ctrl"
			});
	});

	app.controller("AppCtrl", function ($scope, $routeParams) {
		$scope.model = {
			message: $routeParams.message
		};
	});

	app.controller("App2Ctrl", function ($scope, $routeParams) {
		$scope.model = {
			message: "Address: " +
				$routeParams.country + ", " +
				$routeParams.state + ", " +
				$routeParams.city
		};
	});
}());