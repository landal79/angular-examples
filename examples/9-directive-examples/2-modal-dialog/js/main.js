'use strict';

var app = angular.module('app', [])
    .directive('modalDialog', function () {
        return {
            restrict: 'E',
            scope: {
                title: '@',
                show: '='
            },
            replace: true, // Replace with the template below
            transclude: true, // we want to insert custom content inside the directive
            link: function (scope, element, attrs) {
                scope.dialogStyle = {};
                if (attrs.width)
                    scope.dialogStyle.width = attrs.width;
                if (attrs.height)
                    scope.dialogStyle.height = attrs.height;
                scope.hideModal = function () {
                    scope.show = false;
                };
            },
            templateUrl: 'templates/dialog.html' // See below
        };
    }).controller('MyCtrl', ['$scope',
        function ($scope) {
            $scope.modalShown = false;
            $scope.toggleModal = function () {
                $scope.modalShown = !$scope.modalShown;
            };
}]);