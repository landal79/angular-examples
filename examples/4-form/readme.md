# Angular Form


## Form Validation 
Angular input validation:  
 
    <input type="text"
        ng-model="{string}"
        [name="{string}"]
        [required]
        [ng-required="{string}"]
        [ng-minlength="{number}"]
        [ng-maxlength="{number}"]
        [ng-pattern="{string}"]
        [ng-change="{string}"]>


Angular validation objects

    //[formName].[inputFieldName].property
    
    myForm.email1.$pristine;
    // Boolean. True if the user has not yet modified the form.
    myForm.email1.$dirty
    // Boolean. True if the user has already modified the form.
    myForm.email1.$valid
    // Boolean.True if the the form passes the validation.
    myForm.email1.$invalid
    // Boolean. True if the the form doesn't pass the validation.
    myForm.email1.$error
    // Object
    [/code]
    The object [...].$error could be something like this:
    [code lang="js"][/code]
    { required: false, pattern:true }
        // or { required: false, email:true }

Here are some link for Form validation  
 - [blog.brunoscopelliti.com](http://blog.brunoscopelliti.com/form-validation-the-angularjs-way)  
 - [www.ng-newsletter.com](http://www.ng-newsletter.com/posts/validations.html)
