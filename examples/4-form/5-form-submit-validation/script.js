var app = angular.module('myApp', []);
app.controller('signupController', ['$scope',
	function ($scope) {
		$scope.submitted = false;
		$scope.signupForm = function () {
			if ($scope.signup_form.$valid) {
				console.info('valid input!');
			} else {
				$scope.signup_form.submitted = true;
			}
		}
}]);