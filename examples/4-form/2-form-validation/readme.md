To allow styling of form as well as controls, ngModel add these CSS classes:

ng-valid
ng-invalid
ng-pristine
ng-dirty
The following example uses the CSS to display validity of each form control. In the example both user.name and user.email are required, but are rendered with red background only when they are dirty. This ensures that the user is not distracted with an error until after interacting with the control, and failing to satisfy its validity.
