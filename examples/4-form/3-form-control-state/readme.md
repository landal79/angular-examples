A form is an instance of FormController. The form instance can optionally be published into the scope using the name attribute.

Similarly, an input control that has the api/ng.directive:ngModel directive holds an instance of NgModelController. Such a control instance can be published as a property of the form instance using the name attribute on the input control. The name attribute specifies the name of the property on the form instance.

This implies that the internal state of both the form and the control is available for binding in the view using the standard binding primitives.

This allows us to extend the above example with these features:

RESET button is enabled only if form has some changes
SAVE button is enabled only if form has some changes and is valid
custom error messages for user.email and user.agree
