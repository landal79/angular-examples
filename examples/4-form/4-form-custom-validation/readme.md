Angular provides basic implementation for most common html5 input types: (text, number, url, email, radio, checkbox), as well as some directives for validation (required, pattern, minlength, maxlength, min, max).

Defining your own validator can be done by defining your own directive which adds a custom validation function to the ngModel controller. To get a hold of the controller the directive specifies a dependency as shown in the example below. The validation can occur in two places:

Model to View update - Whenever the bound model changes, all functions in NgModelController#$formatters array are pipe-lined, so that each of these functions has an opportunity to format the value and change validity state of the form control through NgModelController#$setValidity.

View to Model update - In a similar way, whenever a user interacts with a control it calls NgModelController#$setViewValue. This in turn pipelines all functions in the NgModelController#$parsers array, so that each of these functions has an opportunity to convert the value and change validity state of the form control through NgModelController#$setValidity.

In the following example we create two directives.

The first one is integer and it validates whether the input is a valid integer. For example 1.23 is an invalid value, since it contains a fraction. Note that we unshift the array instead of pushing. This is because we want to be first parser and consume the control string value, as we need to execute the validation function before a conversion to number occurs.

The second directive is a smart-float. It parses both 1.2 and 1,2 into a valid float number 1.2. Note that we can't use input type number here as HTML5 browsers would not allow the user to type what it would consider an invalid number such as 1,2.