'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', []).controller('stageController', ['$scope',
	function ($scope) {
		$scope.username1 = 'Peter Parker';
		$scope.email1 = 'pparker@gmail.com';

		$scope.submitForm = function () {
			console.info($scope.username1 + ' ' + $scope.email1 );
		}
}]);