Implementing custom form controls (using ngModel)

Angular implements all of the basic HTML form controls (input, select, textarea), which should be sufficient for most cases. However, if you need more flexibility, you can write your own form control as a directive.

In order for custom control to work with ngModel and to achieve two-way data-binding it needs to:

implement $render method, which is responsible for rendering the data after it passed the NgModelController#$formatters,
call $setViewValue method, whenever the user interacts with the control and model needs to be updated. This is usually done inside a DOM Event listener.
See $compileProvider.directive for more info.

The following example shows how to add two-way data-binding to contentEditable elements.