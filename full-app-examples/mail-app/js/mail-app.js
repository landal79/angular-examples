var myApp = angular.module('mailApp', ['ngRoute'])
.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/inbox.html',
            controller: 'InboxController',
            controllerAs: 'inbox',
            name: 'inbox'
        })
        .when('/message/:id', {
            templateUrl: 'views/message.html',
            controller: 'MessageController',
            controllerAs: 'message',
            name: 'inbox'
        })
        .when('/compose', {
            templateUrl: 'views/compose.html',
            controller: 'MessageController',
            controllerAs: 'compose',
            name: 'compose'
        })
        .when('/settings', {
            templateUrl: 'views/settings.html',
            controller: 'SettingsController',
            controllerAs: 'settings',
            name: 'settings'
        })
        .when('/about', {
            templateUrl: 'views/about.html',
            controller: 'AboutController',
            controllerAs: 'about',
            name: 'about'
        })
        .otherwise({
            redirectTo: '/'
        });
}).factory('messages', function ($http) {
    return $http
        .get('messages.json')
        .then(function (response) {
            return response.data;
        });
}).controller('InboxController', function (messages) {
    var self = this;
    messages.then(function (messages) {
        self.messages = messages;
    });
}).controller('NavController', function ($route) {
    this.is = function (title) {
        if (!$route.current) {
            return false;
        }
        return $route.current.name == title;
    }
}).controller('MessageController', function ($routeParams, messages) {
    var self = this;
    messages.then(function (messages) {
        self.detail = messages[$routeParams.id];
    });
}).controller('SettingsController', function () {
    // TODO
}).controller('ComposeController', function () {
    // TODO
}).controller('AboutController', function () {
    // TODO
});
