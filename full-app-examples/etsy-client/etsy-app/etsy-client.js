var etsyModule = angular.module('etsyApp', ['ngSanitize']);

etsyModule.controller('ItemsCtrl', function () {

	this.items = [
		{
			"title": "Horses French Barrette Clip 80mm",
			"description": "I just made more.  Should be shipping by November 4th.  Thank you for your patience.\r\n\r\nMake a statement. This is THEE most beautifully sculpted Horse Barrette you will ever see.  The detail and dimension is truly impressive.  The 80mm  genuine French Barrette Clip used is suitable for medium to thicker hair.  Hand soldered and hand finished.  \r\n1 1/2&quot; x 3 3/4&quot;\r\n\r\nMatching Brooch\r\nhttps://www.etsy.com/listing/93621600/horse-brooch\r\n\r\nThank you for visiting my shop and please come again.  Many more Brooches and Hair Accessories to see soon.  My collection includes all living creatures, careers, hobbies and fantasy,  interpreted using mixed metals of copper silver and brass. Designed and hand made by me in the USA.",
			"price": "20.00",
			"currency_code": "USD",
			"quantity": 2,
			"tags": [
            "barrette",
            "barrettes",
            "french barrette",
            "french barrettes",
            "horse barrette",
            "horse barrettes",
            "horse hair clip",
            "horse jewelry",
            "horse gift",
            "horses barrette",
            "horse french clip",
            "horse french clips",
            "horse accessory"
         ],
			"materials": [
            "metalworks",
            "mixed metal",
            "copper",
            "silver",
            "brass"
         ],
			"url": "http://www.etsy.com/listing/111651504/horses-french-barrette-clip-80mm?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "blue, navy blue, tan , grey, royal blue seat belt Messenger bag",
			"description": "Seat belt messenger bag with adjustable strap 16&quot;x3&quot;x12&quot;",
			"currency_code": "USD",
			"quantity": 0,
			"tags": [],
			"materials": [],
			"url": "http://www.etsy.com/listing/172253089/blue-navy-blue-tan-grey-royal-blue-seat?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "blemish sale - canvas bag - I Like Big Books and I Cannot Lie - tote bag",
			"description": "Blemishes/Seconds are the bags that are ever so slightly less than perfect - the print may be slightly faded in some small areas or the image is over a fold or a little too high or too low on the bag. The wording is still very easily readable. You get big savings because of my mistake!\r\n\r\nfor the full-quality version of this tote, click here:  \r\nhttps://www.etsy.com/listing/112798486/canvas-bag-i-like-big-books-and-i-cannot?ref=shop_home_feat\r\n\r\n* * * * * * * * * * * * * * * * * * * * * \r\n\r\nperfect tote for the book lover! (don&#39;t forget your book club friends!)\r\n\r\nfeatured in the June 2012 issue of Cincinnati Magazine!\r\n\r\n- Canvas Tote Bag\r\n- 100% Cotton Natural Canvas with Woven Handles\r\n- Screen printed by hand with the image shown above (&quot;I Like Big Books & I Cannot Lie&quot;)\r\n- Measures 14-3/4&quot; x 14&quot; x 4&quot;\r\n\r\nMakes a great gift for Brides, Bridesmaids, Mommies, Mommies-to-Be, Teachers, Students, Kids, and anyone with a little attitude!\r\n\r\nCanvas tote is great for use as a book bag, purse, grocery tote, diaper bag, beach bag, toy bag, and more! Great bag to reuse to help the environment!",
			"price": "9.00",
			"currency_code": "USD",
			"quantity": 4,
			"tags": [
            "canvas tote bag",
            "i like big books",
            "big books bag",
            "canvas tote bags",
            "canvas bag totes",
            "i like big books bag",
            "big books canvas bag",
            "canvas tote book bag",
            "tote bag",
            "large tote bag",
            "canvas bag",
            "large canvas bag",
            "Weekend canvas tote"
         ],
			"materials": [
            "cotton canvas",
            "Screen printing ink"
         ],
			"url": "http://www.etsy.com/listing/155464565/blemish-sale-canvas-bag-i-like-big-books?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "RING - Adjustable -Copper and Magnesite",
			"description": "A large bold ring crafted from copper wire and a large magnesite (much like turpuoise) bead....goes twice around the finger to support and give comfort.  Ring is 1 1/8&quot; across and is totally adjustable to fit any finger. A wise and pleasing choice.",
			"price": "22.00",
			"currency_code": "USD",
			"quantity": 1,
			"tags": [
            "jewelry",
            "jewellery",
            "ring",
            "magnestine",
            "aqua",
            "copper",
            "handmade",
            "adjustable",
            "natural stone",
            "handcrafted",
            "adjustable ring",
            "free shipping"
         ],
			"category_path": [
            "Jewelry",
            "Ring"
         ],
			"category_path_ids": [
            68887482,
            69152047
         ],
			"materials": [
            "copper",
            "magnestine"
         ],
			"shop_section_id": 10674481,
			"featured_rank": null,
			"state_tsz": 1386526343,
			"url": "http://www.etsy.com/listing/172238352/ring-adjustable-copper-and-magnesite?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "All Natural Organic Whitening Toothpaste",
			"description": "In the organic world toothpaste is a controversial product whose ingredients are constantly up for debate. Most contain dyes, sulfates, silica, fluoride, flavorings and other ingredients that can have an adverse effect on our teeth and gums. This is a chemical free blend that will leave your teeth feeling polished and very healthy. \r\n\r\n In this toothpaste I combine baking soda, sea salt, calcium, essential oils and coconut oil which has a wonderful and naturally occurring anti-bacterial properties and stops tooth decay. These ingredients promote overall mouth and body health and do not have any adverse effects. Texture is similar to regular tube toothpastes and oils are undetectable. Ingredients are added for their health benefits and paste is pleasant to use and leaves your mouth feeling healthy and fresh. This is a wonderful toothpaste to compliment oil pulling. \r\n\r\n Paste comes in a 2 oz. GLASS JAR with wooden applicator and comes flavored with Sweet Orange essential oil or Peppermint with Rosemary. \r\n\r\nIngredients/Benefits:\r\n\r\nBaking Soda: Whitens teeth naturally, freshens breath, neutralizes mouth Ph and polishes teeth without scratching tooth surface. \r\n\r\nSea Salt: Strengthens teeth by adding necessary minerals and polishes teeth. Also a natural source of fluoride to prevent cavities. \r\n\r\nCalcium: Strengthens teeth, adds an essential mineral to the surface, thickens paste and  acts as an extra polishing agent. \r\n\r\nOrganic Coconut Oil: Stops tooth decay, naturally anti-fungal and antibacterial, whitens teeth, is packed full of vitamins and minerals to strengthen teeth, freshens breath and aids in stopping receding gums from gingivitis and advanced periodontal disease. \r\n\r\nRosemary Essential Oil: Prevents bad breath and plaque, naturally astringent and antibacterial, keeps the gums and teeth healthy. If pregnant, Rosemary essential oil should be avoided. Message me and I&#39;ll omit it. Separation in product is normal. \r\n\r\n\r\nShop Info: \r\n\r\nWe believe that your skin is just as vital as any other organ in your body. Here at Butter Me Up Organics we like to say, &quot;Don&#39;t put anything on your body that you wouldn&#39;t put in it.&quot; So, on that note, all of our products are completely edible. They may not be so tasty, however, but everything we use are non- toxic premium ingredients. Can you say that about the products you&#39;re currently using? \r\n\r\nOther Info:\r\n\r\nOrganic\r\n\r\nCompletely cruelty free\r\n\r\nNo sulfates, phthalates or parabens…ever\r\n\r\nNo synthetic dyes or fragrances\r\n\r\nCompletely all natural and safe\r\n\r\nZero waste company\r\n\r\nPackaging upcycled when possible\r\n\r\nEverything is made with love &lt;3",
			"price": "5.00",
			"currency_code": "USD",
			"quantity": 9,
			"tags": [
            "oral health",
            "toothpaste",
            "all natural",
            "organic",
            "mouth",
            "care",
            "teeth",
            "gingivitis",
            "oral",
            "dentist",
            "paste",
            "hygeine",
            "bad breath"
         ],
			"materials": [
            "baking soda",
            "sea salt",
            "calcium",
            "organic",
            "whiteining",
            "gums",
            "eco friendly",
            "periodontal",
            "disease",
            "white",
            "tooth",
            "pulling",
            "oil"
         ],
			"url": "http://www.etsy.com/listing/102071469/all-natural-organic-whitening-toothpaste?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "Handmade cotto wool pillow cat",
			"description": "Totally handmade pillow with a small cat on it.\r\nOn the backside is red\r\nSizes: 40 x in English size range",
			"price": "180.00",
			"currency_code": "EUR",
			"quantity": 1,
			"tags": [
            "pillow",
            "cat",
            "pillow cat",
            "handmade pillow",
            "handmade pillow",
            "handmade",
            "handcraft",
            "red cat",
            "house",
            "handmade house"
         ],
			"materials": [],
			"url": "http://www.etsy.com/listing/172238350/handmade-cotto-wool-pillow-cat?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      },
		{
			"title": "Handmade Organic Deodorant",
			"description": "Deodorant is quite possibly the most carcinogenic product that we use on our bodies and we use it daily. To find an effective alternative is very difficult and could be very embarrassing if it doesn&#39;t work. I have tried many name brand alternatives and have found them to be ineffective and make you feel very slimy or sweaty. Yuck. Oddly enough, it was this somewhat simple recipe with effective essential oils that I found to work great and decided to package it myself so others could use it. \r\n \r\n Just rub a small amount under arms after shower and you&#39;re ready to go for the whole day! \r\nThis deodorant really works, check out our feedback! It&#39;s all I use and it lasts all day. Money back guarantee. \r\n\r\nTalc free. Aluminum free. Paraben Free. Propylene Glycol Free. Triclosan Free. \r\n\r\n I combine baking soda, that alone works great for a time, with coconut oil which is naturally anti-bacterial, anti-fungal and does not leave a greasy feel. I add antibacterial essential oils for effectiveness and their awesome scent and viola! An all natural and effective deodorant. I LOVE this product, it works great and smells awesome! \r\n\r\nWhat others are saying:\r\n\r\n&quot;I put this on before bed and there is absolutely no need to reapply in the morning. This deodorant held up through 104 degree weather in northern California. I am so happy I have found this and will continue to use this forever!&quot;\r\n\r\n&quot;This stuff works! My husband is particularly stinky and this leaves him smelling fresh all day- good job on figuring this one out- there is hope for him! I will definitely be back for more of your wonderful stuff!&quot;\r\n\r\n&quot;This stuff works fabulous, I sit here 24 hours after I applied the deodorant and still fresh as a daisy , which is really amazing. More amazing is that my wife was raving about it before I even got to try it, and she resists anything organic&quot;\r\n\r\n&quot;This is great deodorant! Helped control odor on a hot, sunny day -- so far it&#39;s working way better for me than the health-food store stuff. Smells really nice, too. :)&quot;\r\n\r\n&quot;I didn&#39;t know what to expect with natural deodorant but it WORKS! I put some on in the morning and by night, it&#39;s still going strong&quot;\r\n\r\n\r\n\r\nComes in a 4 oz. clear glass jar that can be reused or recycled.\r\n\r\nI know shipping is expensive! I&#39;m working on a way to get it down, but this product is almost 11oz. on it&#39;s own, which means I&#39;m paying more than I&#39;m charging for shipping. \r\n\r\n It does fit in a small flat rate box, so, if you purchase other items and they fit in that box, they ship total for $5.15 and I refund you the shipping of those items. Please understand, I&#39;m charging the lowest possible rates for shipping and will refund the difference if there is any. \r\n\r\nIngredients: Sodium Bicarbonate, Coconut Oil*, Rosemary EO, Jojoba oil*, Grapeseed oil, Sandalwood EO*, Lavender EO*, Cedarwood EO*, Cypress EO*, Fir Needle EO*, Sweet Marjoram EO, Sage EO*\r\n*Indicates Certified Organic   **EO=Essential Oil\r\n\r\nShop Info: \r\n\r\nWe believe that your skin is just as vital as any other organ in your body. Here at Butter Me Up Organics we like to say, &quot;Don&#39;t put anything on your body that you wouldn&#39;t put in it.&quot; So, on that note, all of our products are completely edible. They may not be so tasty, however, but everything we use are non- toxic premium ingredients. Can you say that about the products you&#39;re currently using? \r\n\r\nOther Info:\r\n\r\nOrganic\r\n\r\nCompletely cruelty free\r\n\r\nNo sulfates, phthalates or parabens…ever\r\n\r\nNo synthetic dyes or fragrances\r\n\r\nCompletely all natural and safe\r\n\r\nZero waste company\r\n\r\nPackaging upcycled when possible\r\n\r\nEverything is made with love &lt;3",
			"price": "8.50",
			"currency_code": "USD",
			"quantity": 5,
			"tags": [
            "deodorant",
            "smelly",
            "antipersperant",
            "stink",
            "underarms",
            "armpit",
            "organic",
            "sweaty",
            "sweat",
            "smell",
            "antifungal",
            "BO",
            "natural"
         ],
			"materials": [
            "rosemary",
            "Sandalwood",
            "lavender",
            "coconut",
            "jojoba",
            "oil",
            "body",
            "odor",
            "stinky",
            "under",
            "arm",
            "eco",
            "friendly"
         ],
			"url": "http://www.etsy.com/listing/102073351/handmade-organic-deodorant?utm_source=landaletsyclient&utm_medium=api&utm_campaign=api",
      }
];

});