var etsyModule = angular.module('etsyApp', ['ngSanitize']);

etsyModule.controller('ItemsCtrl', ['$scope', '$http',
	function ($scope, $http) {			
		
		$http.get('json/active.json')
			.success(function (data, status, headers, config) {
				console.info("success:\n" + headers);
				$scope.items = data.results;
			}).
		error(function (data, status, headers, config) {
			console.error("error:\n" + status);
		});
				
}]);