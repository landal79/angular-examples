var etsyModule = angular.module('etsyApp', ['ngSanitize']);

etsyModule.controller('ItemsCtrl', ['$scope', '$http',
	function ($scope, $http) {

		$scope.etsyUrl = 'https://openapi.etsy.com/v2/listings/active.js';	

		$http.jsonp(
			$scope.etsyUrl, {
			params: {
				callback : 'JSON_CALLBACK',
				api_key: '3pd3ist8rduy8hk259rhp6vx'				
			}
		})
			.success(function (data, status, headers, config) {
				console.info("success:\n" + status);
				$scope.items = data.results;
			}).
		error(function (data, status, headers, config) {
			console.error("error:\n" + status);
		});

}]);